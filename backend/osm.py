import sys
import osmium
import shapely.wkb as wkblib
import pickle

import border
import osmobjects
import harversine

wkbfab = osmium.geom.WKBFactory()

class OSMRegion(osmium.SimpleHandler):
    def __init__(self, country_tag, load_ways):
        osmium.SimpleHandler.__init__(self)

        self.country_tag = country_tag
        self.load_ways = load_ways

        self.roundabouts = []
        self.buildings = []
        self.hospitals = []
        self.waterways = []
        self.ways = {}
    
    def get_area_location(self, a):
        try:
            wkb = wkbfab.create_multipolygon(a)
            poly = wkblib.loads(wkb, hex=True)
            centroid = poly.representative_point()
            return (centroid.y, centroid.x)
        except: # It is possible to get an invalid area exception
            print('Invalid area', a)
            return None
    
    def area(self, a):
        if 'junction' in a.tags and a.tags['junction'] == 'roundabout':
            node = self.get_area_location(a)
            if node != None:
                self.roundabouts.append(node)
        elif 'building' in a.tags:
            node = self.get_area_location(a)
            if node != None:
                self.buildings.append(node)
        elif 'amenity' in a.tags and a.tags['amenity'] == 'hospital':
            node = self.get_area_location(a)
            if node != None:
                self.hospitals.append(node)
        elif 'waterway' in a.tags or 'natural' in a.tags or 'water' in a.tags:
            node = self.get_area_location(a)
            if node != None:
                self.waterways.append(node)
    
    def way(self, w):
        if 'junction' in w.tags and w.tags['junction'] == 'roundabout':
            self.roundabouts.append((w.nodes[0].location.lat, w.nodes[0].location.lon))
        elif 'building' in w.tags:
            self.buildings.append((w.nodes[0].location.lat, w.nodes[0].location.lon))
        elif 'amenity' in w.tags and w.tags['amenity'] == 'hospital':
            self.hospitals.append((w.nodes[0].location.lat, w.nodes[0].location.lon))
        elif 'waterway' in w.tags or 'natural' in w.tags or 'water' in w.tags:
            self.waterways.append((w.nodes[0].location.lat, w.nodes[0].location.lon))

        # Saving ALL ways to RAM is not very space-efficient, but the only alternative is to parse the PBF file twice to only get the ways referenced by the border relation, since we can't find the relation without reading the whole file anyway.
        if self.load_ways:
            self.ways[w.id] = osmobjects.MyWay(w)
    
    def relation(self, r):
        if 'ISO3166-1' in r.tags and r.tags['ISO3166-1'] == self.country_tag and 'land_area' in r.tags and r.tags['land_area'] == 'administrative':
            self.border_relation = r
    
    def draw_border(self):
        min_lat = 90.0
        min_lon = 180.0
        max_lat = -90.0
        max_lon = -180.0

        n = 0

        border_ways = []
        for member in self.border_relation.members:
            if member.type == 'w':
                if member.ref in self.ways.keys():
                    border_ways.append(self.ways[member.ref])
                else:
                    print('Way', member.ref, 'not found in file. Are you sure you downloaded the whole country\'s file? (If you only get a few of these errors, you can safely ignore them)')

        del self.ways

        self.border = border.Border(border_ways)
        del border_ways

        return self.border

    def draw_density(self, elements):
        dlat = self.border.max_lat - self.border.min_lat
        dlon = self.border.max_lon - self.border.min_lon

        X_RES = 64
        Y_RES = 64

        l = []
        for x in range(0, X_RES):
            l.append([])
            for y in range(0, Y_RES):
                l[x].append({
                    'count': 0,
                    'x': x / X_RES,
                    'y': 1.0 - y / Y_RES
                })

        for element in elements:
            x = int((element[1] - self.border.min_lon) / dlon * X_RES)
            y = int((element[0] - self.border.min_lat) / dlat * Y_RES)
            if x < X_RES and x >= 0 and y < Y_RES and y >= 0:
                l[x][y]['count'] = l[x][y]['count'] + 1
            else:
                print('Invalid building coordinates:', x, y, '(', element[0], element[1], ')')
        
        l_json = []
        for x in range(0, X_RES):
            for y in range(0, Y_RES):
                l_json.append(l[x][y])
        
        return l_json


    def draw_densities(self):
        return {
            'roundabouts': {
                'points': self.draw_density(self.roundabouts)
            },
            'buildings': {
                'points': self.draw_density(self.buildings)
            },
            'hospitals': {
                'points': self.draw_density(self.hospitals)
            },
            'waterways': {
                'points': self.draw_density(self.waterways)
            }
        }
